﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using SchematiX.Helper;
namespace SchematiX.MVVM
{
    public class UINPN : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                GeometryGroup gg = new GeometryGroup();
                gg.FillRule = FillRule.Nonzero;

                switch (Part.Direction)
                {
                    case Helper.Helper.Direction.East: //(Width=64; Height=96)
                        //basisistrich
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[0].Location,
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height / 2)));
                        //vertical strich zwischen emittor und collector
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Width / 4),
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height * 5 / 6)));
                        //emitterstrich
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Width),
                                Part.Plugs[1].Location));
                        //pfeil
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width / 4, Part.Location.Y + Part.Size.Width / 8)));
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width / 8, Part.Location.Y + Part.Size.Width / 4)));
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 4, Part.Location.Y + Part.Size.Width / 8),
                                new Point(Part.Location.X + Part.Size.Width / 8, Part.Location.Y + Part.Size.Width / 4)));
                        //collecotr
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Width / 2),
                                Part.Plugs[2].Location));
                        break;

                    case Helper.Helper.Direction.South:    //(Width=96; Height=64)
                        //basisrich
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height / 2),
                                Part.Plugs[0].Location));
                        //horizontaler strih zwischen emittor und collector
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Height / 4, Part.Location.Y + Part.Size.Height / 2),
                                new Point(Part.Location.X + Part.Size.Width * 5 / 6, Part.Location.Y + Part.Size.Height / 2)));
                        //emittor
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Height / 2, Part.Location.Y + Part.Size.Height / 2),
                                Part.Plugs[1].Location));
                        //pfeil
                        gg.Children.Add(
                             new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width * 5 / 6, Part.Location.Y + Part.Size.Height / 8)));
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width * 11 / 12, Part.Location.Y + Part.Size.Height / 4)));
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width * 5 / 6, Part.Location.Y + Part.Size.Height / 8),
                                new Point(Part.Location.X + Part.Size.Width * 11 / 12, Part.Location.Y + Part.Size.Height / 4)));

                        //collector strich
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Height, Part.Location.Y + Part.Size.Height / 2),
                                Part.Plugs[2].Location));
                        break;

                    case Helper.Helper.Direction.West:    //(Width=64; Height=96)
                        //basisstrich
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[0].Location,
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height / 2)));
                        //Horizontaler Strich zwischen Emittor und Collector
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Width / 4),
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height * 5 / 6)));
                        //Emitterstrich 
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Width / 2),
                               Part.Plugs[1].Location));
                        //pfeil
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width * 3 / 4, Part.Location.Y + Part.Size.Height * 11 / 12)));
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width * 7 / 8, Part.Location.Y + Part.Size.Height * 5 / 6)));
                        gg.Children.Add(
                            new LineGeometry(
                            new Point(Part.Location.X + Part.Size.Width * 3 / 4, Part.Location.Y + Part.Size.Height * 11 / 12),
                            new Point(Part.Location.X + Part.Size.Width * 7 / 8, Part.Location.Y + Part.Size.Height * 5 / 6)));
                        //Collector strich
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Width),
                                Part.Plugs[2].Location));

                        break;
                    case Helper.Helper.Direction.North:    //(Width=96; Height=64)
                        //Basisstrich
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[0].Location,
                                new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height / 2)));
                        //waagrechter strich collecotr emittor
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Height / 4, Part.Location.Y + Part.Size.Height / 2),
                                new Point(Part.Location.X + Part.Size.Width * 5 / 6, Part.Location.Y + Part.Size.Height / 2)));
                        //Emitterstrich 
                        gg.Children.Add(
                             new LineGeometry(
                                 new Point(Part.Location.X + Part.Size.Height, Part.Location.Y + Part.Size.Height / 2),
                                 Part.Plugs[1].Location));
                        //pfeil
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width * 1 / 12, Part.Location.Y + Part.Size.Height * 3 / 4)));
                        gg.Children.Add(
                            new LineGeometry(
                                Part.Plugs[2].Location,
                                new Point(Part.Location.X + Part.Size.Width * 1 / 6, Part.Location.Y + Part.Size.Height * 7 / 8)));
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Width * 1 / 12, Part.Location.Y + Part.Size.Height * 3 / 4),
                                new Point(Part.Location.X + Part.Size.Width * 1 / 6, Part.Location.Y + Part.Size.Height * 7 / 8)));
                        //collector strich
                        gg.Children.Add(
                            new LineGeometry(
                                new Point(Part.Location.X + Part.Size.Height / 2, Part.Location.Y + Part.Size.Height / 2),
                                Part.Plugs[2].Location));
                        break;
                    default:
                        break;

                }
                //gg.Children.Add(new LineGeometry(Part.Junctions[0].Location, Part.Plugs[0].Location));
                //gg.Children.Add(new LineGeometry(Part.Junctions[1].Location, Part.Plugs[1].Location));
                //gg.Children.Add(new LineGeometry(Part.Junctions[2].Location, Part.Plugs[2].Location));
                p.Data = gg;
                return p;
            }
        }
    }
}
