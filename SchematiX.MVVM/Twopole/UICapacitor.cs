﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchematiX.MVVM
{
    public class UICapacitor : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                p.Fill = Stroke;
                GeometryGroup gg = new GeometryGroup();

                if (Part.Orientation == System.Windows.Controls.Orientation.Horizontal)
                {
                    gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X + 38, Part.Location.Y + 12.5, 8, 25)));
                    gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X + 53, Part.Location.Y + 12.5, 8, 25)));

                    gg.Children.Add(new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 38, Part.Location.Y + 25)));
                    gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 62, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)));
                }
                else
                {
                    gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X + 12.5, Part.Location.Y + 38, 25, 8)));
                    gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X + 12.5, Part.Location.Y + 53, 25, 8)));

                    gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y), new Point(Part.Location.X + 25, Part.Location.Y + 38)));
                    gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y + 62), new Point(Part.Location.X + 25, Part.Location.Y + 100)));
                }

                p.Data = gg;
                return p;
            }

        }
    }
}