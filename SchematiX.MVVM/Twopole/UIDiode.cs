﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchematiX.MVVM
{
    public class UIDiode : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                GeometryGroup gg = new GeometryGroup();
                gg.FillRule = FillRule.Nonzero;
                //gg.Children.Add(new LineGeometry(Part.Location.X, Part.Location.Y));

                switch (Part.Direction)
                {
                    case Helper.Helper.Direction.East:
                        gg.Children.Add(new LineGeometry(
                        new Point(Part.Location.X + 35, Part.Location.Y + 12.5),           //linke linie vertikal
                        new Point(Part.Location.X + 35, Part.Location.Y + 37.5)));

                        gg.Children.Add(new LineGeometry(
                        new Point(Part.Location.X + 35, Part.Location.Y + 12.5),      //obere querlinie
                        new Point(Part.Location.X + 65, Part.Location.Y + 25)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 35, Part.Location.Y + 37.5),  //untere querlinie
                            new Point(Part.Location.X + 65, Part.Location.Y + 25)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 65, Part.Location.Y + 12.5),  //rechte vertikale
                            new Point(Part.Location.X + 65, Part.Location.Y + 37.5)));
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X, Part.Location.Y + 25),            //horizontale   
                            new Point(Part.Location.X + 100, Part.Location.Y + 25)));
                        break;

                    case Helper.Helper.Direction.North:
                        gg.Children.Add(new LineGeometry(
                             new Point(Part.Location.X + 12.5, Part.Location.Y + 35),
                             new Point(Part.Location.X + 37.5, Part.Location.Y + 35)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X +12.5, Part.Location.Y + 35),
                            new Point(Part.Location.X + 25, Part.Location.Y + 65)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 37.5, Part.Location.Y + 35),
                            new Point(Part.Location.X + 25, Part.Location.Y + 65)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 12.5, Part.Location.Y + 65),
                            new Point(Part.Location.X + 37.5, Part.Location.Y + 65)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 25, Part.Location.Y),
                            new Point(Part.Location.X + 25, Part.Location.Y + 100)));
                        break;

                    case Helper.Helper.Direction.West:
                        gg.Children.Add(new LineGeometry(
                         new Point(Part.Location.X + 35, Part.Location.Y + 12.5),           //linke linie vertikal
                         new Point(Part.Location.X + 35, Part.Location.Y + 37.5)));

                        gg.Children.Add(new LineGeometry(
                        new Point(Part.Location.X + 35, Part.Location.Y + 25),      //obere querlinie
                        new Point(Part.Location.X + 65, Part.Location.Y + 12.5)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 35, Part.Location.Y + 25),  //untere querlinie
                            new Point(Part.Location.X + 65, Part.Location.Y + 37.5)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 65, Part.Location.Y + 12.5),  //rechte vertikale
                            new Point(Part.Location.X + 65, Part.Location.Y + 37.5)));
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X, Part.Location.Y + 25),            //horizontale   
                            new Point(Part.Location.X + 100, Part.Location.Y + 25)));
                        break;

                    case Helper.Helper.Direction.South:
                        gg.Children.Add(new LineGeometry(
                             new Point(Part.Location.X + 12.5, Part.Location.Y + 35),
                             new Point(Part.Location.X + 37.5, Part.Location.Y + 35)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 25, Part.Location.Y + 35),
                            new Point(Part.Location.X + 12.5, Part.Location.Y + 65)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 25, Part.Location.Y + 35),
                            new Point(Part.Location.X + 37.5, Part.Location.Y + 65)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 12.5, Part.Location.Y + 65),
                            new Point(Part.Location.X + 37.5, Part.Location.Y + 65)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + 25, Part.Location.Y),
                            new Point(Part.Location.X + 25, Part.Location.Y + 100)));
                        break;
                }
                p.Data = gg;
                
                return p;
            }
        }
    }
}
