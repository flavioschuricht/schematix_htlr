﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using SchematiX.Model;
using static SchematiX.Helper.Helper;

namespace SchematiX.MVVM
{
    public class UIPart : NotifyPropertyChangedBase
    {
        private Part part;
        private Brush stroke = Brushes.Blue;           //Hier kann die Farbe aller Bauteile geändert werden
        private double strokethickness = 1;
        private bool dashes = false;

        public Direction Orientation
        {
            get { return part.Direction; }
            set {
                part.Direction = value;
                OnPropertyChanged("Path");
            }
        }
        public Direction Direction
        {
            get { return part.Direction; }
            set { part.Direction = value;
                OnPropertyChanged("Path");
            }
        }
        
        public Part Part
        {
            get { return part; }
            set
            {
                part = value;
                //foreach (Node item in part.Junctions)
                //{
                //    item.PropertyChanged += Junction_PropertyChanged;
                //}
                foreach (Node item in part.Plugs)
                {
                    item.PropertyChanged += Plug_PropertyChanged;
                }
                OnPropertyChanged("Name");
                OnPropertyChanged("Location");
                OnPropertyChanged("Path");
            }
        }

        private void Plug_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Location");
            OnPropertyChanged("Path");
        }

        public string Name
        {
            set
            {
                part.Name = value;
                OnPropertyChanged("Name");
            }

            get { return part.Name; }
        }

        public Vector LabelOffset
        {
            set
            {
                part.LabelOffset = value;
                OnPropertyChanged("LabelOffset");
            }
            get => part.LabelOffset;
        }

        public Location Location
        {
            set
            {
                part.Location = value;
                OnPropertyChanged("Path");
                OnPropertyChanged("Location");
            }
            get
            {
                return part.Location;
            }
        }


        public virtual Path Path
        {
            get
            {
                Path p = new Path();
                p.Stroke = stroke;
                p.StrokeThickness = strokethickness;
                GeometryGroup gg = new GeometryGroup();
                gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X, Part.Location.Y, 100, 50)));

                if (dashes)
                {
                    DoubleCollection dashes = new DoubleCollection();
                    dashes.Add(4);
                    dashes.Add(1);
                    p.StrokeDashArray = dashes;
                    p.StrokeDashCap = PenLineCap.Round;
                    p.StrokeDashArray[1] = 2;
                }

                p.Data = gg;
                return p;
            }
        }

        public Brush Stroke
        {
            get
            {
                return stroke;
            }
            set
            {
                stroke = value;
                OnPropertyChanged("Path");
            }
        }

        public UIPart()
        {
            
        }

        public virtual void SetOffset(Vector offset)
        {
            Location = Part.Location + offset;
        }
    }
}
