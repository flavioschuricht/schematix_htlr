﻿using System;
using System.ComponentModel;

namespace SchematiX.Model
{
    public class NotifyPropertyChangedBase : INotifyPropertyChanged, IObservable<Node>
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public IDisposable Subscribe(IObserver<Node> observer)
        {
            throw new NotImplementedException();
        }
    }
}