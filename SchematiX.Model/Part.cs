﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using static SchematiX.Helper.Helper;

namespace SchematiX.Model
{
    public abstract class Part : NotifyPropertyChangedBase
    {
        protected static uint index = 0;
        private Location location = new Location(0, 0);
        private Vector labelOffset;
        private List<Node> plugs = new List<Node>();
        private Size size;
        private Direction direction;
        private Orientation orientation;
        public Size BaseSize { get; set; } = new Size(50, 50);

        public Orientation Orientation {
            get => orientation;
            set => orientation = value;
        }

        public Direction Direction
        {
            get => direction;
            set
            {
                direction = value;
                switch (value)
                {
                    case Direction.East:
                    case Direction.West:
                        Size = new Size(BaseSize.Width, BaseSize.Height);
                        labelOffset = new Vector(0, -30);
                        Orientation = Orientation.Horizontal;
                        break;
                    case Direction.North:
                    case Direction.South:
                        Size = new Size(BaseSize.Height, BaseSize.Width);
                        labelOffset = new Vector(10, 10);
                        Orientation = Orientation.Vertical;
                        break;
                    default:
                        break;
                }
                AdjustPlugs();
            }
        }

        public virtual Size Size
        {
            get { return size; }
            set { size = value; }
        }

        /// <summary>
        /// upper left corner of part
        /// </summary>
        public virtual Location Location
        {
            get => location;
            set
            {
                location = value;
            }
        }

        /// <summary>
        /// Offset of label to location
        /// </summary>
        public Vector LabelOffset
        {
            get => labelOffset;
            set
            {
                labelOffset = value;
            }
        }

        public string Name { get; set; }
        public double Value { get; set; }

        // Connections at part
        public List<Node> Plugs
        {
            get { return plugs; }
            set { plugs = value; }
        }

        public Part()
        {
            index++;
            Direction = Direction.East;
        }

        public string JunctionsAndPlugs()
        {
            string s = "";
            s += $"{Plugs.Count};";
            foreach (Node item in Plugs)
            {
                s += $"Plug;{item.Location.X};{item.Location.Y};";
            }
            return s;
        }

        public string Serialize()
        {
            string s = $"{GetType()};{Name};{Value};{Location.X};{Location.Y};{LabelOffset.X};{LabelOffset.Y};{LabelOffset.Length};{Orientation};{Direction};{BaseSize};{Size};{JunctionsAndPlugs()}";
            return s;
        }

        public static Part Parse(string[] data)
        {
            Part part;
            switch (data[0].ToString())
            {
                case "SchematiX.Model.Resistor":
                    part = new Resistor();
                    break;
                case "SchematiX.Model.Capacitor":
                    part = new Capacitor();
                    break;
                case "SchematiX.Model.Diode":
                    part = new Diode();
                    break;
                case "SchematiX.Model.Voltage":
                    part = new Voltage();
                    break;
                case "SchematiX.Model.Junction":
                    part = new Junction();
                    break;
                case "SchematiX.Model.Current":
                    part = new Current();
                    break;
                case "SchematiX.Model.Elko":
                    part = new Elko();
                    break;
                case "SchematiX.Model.FET":
                    part = new FET();
                    break;
                case "SchematiX.Model.NPN":
                    part = new NPN();
                    break;
                case "SchematiX.Model.PNP":
                    part = new PNP();
                    break;
                //case "SchematiX.Model.TBipolar":
                //    part = new UIResistor();
                //    break;
                //case "SchematiX.Model.TFieldEffect":
                //    part = new ();
                //    break;
                default:
                    part = new Resistor();
                    break;
            }

            part.Name = data[1];
            part.Value = double.Parse(data[2]);
            part.Location.X = double.Parse(data[3]);
            part.Location.Y = double.Parse(data[4]);
            part.LabelOffset = new Vector(double.Parse(data[5]), double.Parse(data[6]));
            if (data[8] == "Horizontal") { part.Orientation = System.Windows.Controls.Orientation.Horizontal; }
            else { part.Orientation = System.Windows.Controls.Orientation.Vertical; }
            switch (data[9])
            {
                case "North":
                    part.Direction = Direction.North;
                    break;
                case "South":
                    part.Direction = Direction.South;
                    break;
                case "West":
                    part.Direction = Direction.West;
                    break;
                case "East":
                    part.Direction = Direction.East;
                    break;
                default:
                    break;
            }
            part.BaseSize = new Size(double.Parse(data[10]), double.Parse(data[11]));
            part.Size = new Size(double.Parse(data[12]), double.Parse(data[13]));
            //int x = 0;
            //int y = 14;
            //part.Plugs.Clear();
            /*for (int i = 0; i < double.Parse(data[y]); i += 3)
            {
                part.Plugs[x] = new Node(new Location(double.Parse(data[i + y + 2]), double.Parse(data[i + y + 3])));
                x++;
            }*/

            return part;
        }

        protected abstract void AdjustPlugs();
    }
}
