﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchematiX.Model
{
    public class QuadriPole : Part
    {
        protected override void AdjustPlugs()
        {
            Plugs = new List<Node>
            {
                new Node( new Location(Location.X, Location.Y)),
                new Node( new Location(Location.X + Size.Width, Location.Y)),
                new Node( new Location(Location.X, Location.Y + Size.Height)),
                new Node( new Location(Location.X + Size.Width, Location.Y + Size.Height))
            };
        }

        public override Location Location
        {
            get => base.Location;
            set
            {
                base.Location = value;
                AdjustPlugs();
            }
        }

        public override Size Size
        {
            get => base.Size;
            set
            {
                base.Size = value;
                AdjustPlugs();
            }
        }
    }
}
