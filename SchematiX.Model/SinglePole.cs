﻿using System.Collections.Generic;
using System.Windows;
using static SchematiX.Helper.Helper;

namespace SchematiX.Model
{
    public class SinglePole : Part
    {
        public SinglePole():base()
        {

        }

        protected override void AdjustPlugs()
        {
            Plugs = new List<Node>();

            switch (Direction)
            {
                case Direction.East:
                    Plugs.Add(new Node( new Location(Location.X, Location.Y + Size.Height / 2)));
                    break;
                case Direction.West:
                    Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y + Size.Height / 2)));
                    break;
                case Direction.South:
                    Plugs.Add(new Node(new Location(Location.X + Size.Width / 2, Location.Y)));
                    break;
                case Direction.North:
                    Plugs.Add(new Node(new Location(Location.X + Size.Width / 2, Location.Y + Size.Height)));
                    break;
                default:
                    break;
            }
        }

        public override Location Location
        {
            get => base.Location;
            set
            {
                base.Location = value;
                AdjustPlugs();
            }
        }

        public override Size Size
        {
            get => base.Size;
            set
            {
                base.Size = value;
                AdjustPlugs();
            }
        }
    }
}
