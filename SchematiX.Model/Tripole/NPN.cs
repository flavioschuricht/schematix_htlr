﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchematiX.Model
{
    public class NPN : TriPole
    {
        public string Type { get; set; }
        public NPN() : base()
        {
            Name = $"NPN_{index}";
            BaseSize = new Size(64,96);
        }
    }
}
