﻿using System;
using System.Windows;

namespace SchematiX.Model
{
    public class Resistor : TwoPole
    {
        public double Tolerance { get; set; }

        public Resistor() : base()
        {
            Name = $"R_{index}";
            BaseSize = new Size(70, 20);
        }
    }
}
