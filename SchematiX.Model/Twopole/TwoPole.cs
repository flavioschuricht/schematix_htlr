﻿using System.Collections.Generic;
using System.Windows;
using static SchematiX.Helper.Helper;

namespace SchematiX.Model
{
    public class TwoPole : Part
    {
        public override Location Location
        {
            get => base.Location;
            set
            {
                base.Location = value;
                AdjustPlugs();
            }
        }

        protected override void AdjustPlugs()
        {
            if (Plugs.Count == 0)
            {
                Plugs.Add(new Node(new Location(Location.X, Location.Y + Size.Height / 2)));
                Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y + Size.Height / 2)));
            }
            else
            {
                switch (Direction)
                {
                    case Direction.East:
                    case Direction.West:
                        Plugs[0].Location.X = Location.X;
                        Plugs[0].Location.Y = Location.Y + Size.Height / 2;
                        Plugs[1].Location.X = Location.X + Size.Width;
                        Plugs[1].Location.Y = Location.Y + Size.Height / 2;
                        Plugs[0].Direction = Direction.East;
                        Plugs[1].Direction = Direction.West;
                        break;
                    case Direction.South:
                    case Direction.North:
                        Plugs[0].Location.X = Location.X + Size.Width / 2;
                        Plugs[0].Location.Y = Location.Y;
                        Plugs[1].Location.X = Location.X + Size.Width / 2;
                        Plugs[1].Location.Y = Location.Y + Size.Height;
                        Plugs[0].Direction = Direction.North;
                        Plugs[1].Direction = Direction.South;
                        break;
                    default:
                        break;
                }
            }
        }

        public override Size Size {
            get => base.Size;
            set
            {
                base.Size = value;
                AdjustPlugs();
            }
        }

        public TwoPole() : base()
        {
        }
    }
}
