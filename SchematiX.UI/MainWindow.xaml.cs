﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Win32;
using SchematiX.Model;
using SchematiX.MVVM;
using static SchematiX.Helper.Helper;


namespace CreateUserControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<UIPart> PartCollection { get; set; } = new ObservableCollection<UIPart>();
        protected Point? mouseLastPosition = null;
        UIPart selectedItem = null;
        Brush selectedUItemStroke = null;
        protected Node selectedPlug;

        public MainWindow()
        {
            InitializeComponent();

            canvasMain.MouseDown += CanvasMain_MouseDown;
            canvasMain.MouseMove += CanvasMain_MouseMove_DragPart;
            canvasMain.MouseUp +=
                delegate
                {
                    // signalisiert, dass ziehvorgang ausgesetzt wurde
                    mouseLastPosition = null;
                    if (selectedItem != null)
                    {
                        if (selectedUItemStroke != null)
                        {
                            selectedItem.Stroke = selectedUItemStroke;
                        }
                    }
                    selectedItem = null;
                };

            AddsomeParts();
        }

        private void AddsomeParts()
        {
            Node n1 = new Node(new Location(100, 100));
            Node n2 = new Node(new Location(100, 100));
            Node n3 = new Node(new Location(100, 100));
            Node n4 = new Node(new Location(100, 100));

            FET f1 = new FET()
            {
                Location = new Location(200, 200),
                Direction = Direction.East
            };

            Resistor r1 = new Resistor()
            {
                Location = new Location(100, 150),
                Direction = Direction.North
            };

            Resistor r2 = new Resistor()
            {
                Location = new Location(120, 100),
                Direction = Direction.East
            };
            
            Resistor r3 = new Resistor()
            {
                Location = new Location(120, 150),
                Direction = Direction.East
            };

            Junction j1 = new Junction()
            {
                Location = new Location(100, 100)
            };
            j1.AddConnection(r1.Plugs[0]);
            j1.AddConnection(r2.Plugs[0]);
            j1.AddConnection(r3.Plugs[0]);

            Junction j2 = new Junction()
            {
                Location = new Location(200, 200)
            };
            j2.AddConnection(r1.Plugs[1]);
            j2.AddConnection(r2.Plugs[1]);

            PartCollection.Add(new UIJunction() { Part = j1 });
            PartCollection.Add(new UIJunction() { Part = j2 });
            PartCollection.Add(new UIResistor() { Part = r1 });
            PartCollection.Add(new UIResistor() { Part = r2 });
            PartCollection.Add(new UIResistor() { Part = r3 });

            /*
            Node n1 = new Node(new Location(70, 150));
            Node n2 = new Node(new Location(180, 250));
            Node n3 = new Node(new Location(280, 250));
            Node n4 = new Node(new Location(380, 150));
            Node n5 = new Node(new Location(480, 150));
            Node n6 = new Node(new Location(570, 150));


            Junction j1 = new Junction() { Location = n1.Location };
            j1.Junctions.Add(n1);

            Junction j2 = new Junction() { Location = n2.Location };
            j2.Junctions.Add(n2);

            Junction j3 = new Junction() { Location = n3.Location };
            j3.Junctions.Add(n3);

            Junction j4 = new Junction() { Location = n4.Location };
            j4.Junctions.Add(n4);

            Junction j5 = new Junction() { Location = n5.Location };
            j5.Junctions.Add(n5);

            Junction j6 = new Junction() { Location = n6.Location };
            j6.Junctions.Add(n6);

            FET f1 = new FET()
            {
                Location = new Location(200, 200),
                Direction = Direction.East,
                Junctions = new List<Node> { n1, n2, n3 }
            };

            Resistor r1 = new Resistor()
            {
                Location = new Location(100, 150),
                Direction = Direction.North,
                Junctions = new List<Node> { n1, n3 }
            };

            Resistor r2 = new Resistor()
            {
                Location = new Location(100, 150),
                Direction = Direction.East,
                Junctions = new List<Node> { n1, n3 }
            };

            Capacitor c1 = new Capacitor()
            {
                Location = new Location(200, 250),
                Direction = Direction.West,
                Junctions = new List<Node> { n2, n3 }
            };

            Elko c2 = new Elko()
            {
                Location = new Location(200, 150),
                Direction = Direction.West,
                Junctions = new List<Node> { n2, n3 }
            };

            Voltage v3 = new Voltage()
            {
                Location = new Location(330, 150),
                Junctions = new List<Node> { n3, n4 }
            };

            Current crnt3 = new Current()
            {
                Location = new Location(330, 50),
                Direction = Direction.North,
                Junctions = new List<Node> { n3, n4 }
            };

            Diode d4 = new Diode()
            {
                Location = new Location(420, 150),
                Direction = Direction.East,
                Junctions = new List<Node> { n4, n5 }
            };

            Inductor i5 = new Inductor()
            {
                Location = new Location(500, 150),
                Direction = Direction.North,
                Junctions = new List<Node> { n5, n6 }
            };

            NPN npn1 = new NPN()
            {
                Direction = Direction.West,
                Location = new Location(50, 50),
                Junctions = new List<Node> { n4, n5, n6 }
            };

            PNP p1 = new PNP()
            {
                Direction = Direction.North,
                Location = new Location(500, 500),
                Junctions = new List<Node> { n4, n5, n6 }
            };

            TwoPole tp1 = new TwoPole()
            {
                Location = new Location(500, 350),
                Junctions = new List<Node> { n5, n6 }
            };

            PartCollection.Add(new UIPNP() { Part = p1 });
            PartCollection.Add(new UIJunction() { Part = j1 });
            PartCollection.Add(new UIJunction() { Part = j2 });
            PartCollection.Add(new UIJunction() { Part = j3 });
            PartCollection.Add(new UIJunction() { Part = j4 });
            PartCollection.Add(new UIJunction() { Part = j5 });
            PartCollection.Add(new UIJunction() { Part = j6 });

            PartCollection.Add(new UINPN() { Part = npn1 });
            PartCollection.Add(new UIResistor() { Part = r1 });
            PartCollection.Add(new UIResistor() { Part = r2 });
            PartCollection.Add(new UICapacitor() { Part = c1 });
            PartCollection.Add(new UIElko() { Part = c2 });
            PartCollection.Add(new UIVoltage() { Part = v3 });
            PartCollection.Add(new UICurrent() { Part = crnt3 });
            PartCollection.Add(new UIDiode() { Part = d4 });
            PartCollection.Add(new UIInductor() { Part = i5 });
            PartCollection.Add(new UIPart() { Part = tp1 });
            PartCollection.Add(new UIFET() { Part = f1 });
            */
        }

        private void CanvasMain_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Point position = e.GetPosition(canvasMain);
                var closestItem = itc.Items
                   .OfType<UIPart>()
                   .OrderBy(part => (part.Part.Location - position).Length)
                   .FirstOrDefault();

                var closestPlug = closestItem.Part.Plugs
                    .Select(plug =>
                        new
                        {
                            Plug = plug,
                            Distance = (plug.Location - position).Length
                        })
                    .Where(plug => plug.Distance < 10)
                    .OrderBy(plug => plug.Distance)
                    .Select(plug => plug.Plug)
                    .FirstOrDefault();
                
                // Verbindung von angewähltem Plug auf geklickte Junction
                if (closestItem is UIJunction && selectedPlug != null && closestPlug == null)
                {
                    var targetJunction = closestItem as UIJunction;
                    if(!targetJunction.Part.Plugs.Contains(selectedPlug))
                    {
                        (closestItem as UIJunction).AddConnection(selectedPlug);
                        selectedPlug = null;
                    }
                    return;
                }


                // ein plug wurde ausgewählt
                if (closestPlug != null)
                {
                    // zwischenspeichern
                    if (selectedPlug == null)
                    {
                        selectedPlug = closestPlug;
                        return;
                    }
                    // ...oder junction generieren
                    else
                    {
                        var junction = new Junction
                        {
                            Location = position
                        };
                        junction.AddConnection(closestPlug);
                        junction.AddConnection(selectedPlug);
                        var uiJunction = new UIJunction
                        {
                            Part = junction,
                        };
                        PartCollection.Add(uiJunction);

                        // status zurücksetzen
                        selectedPlug = null;

                        // junction per Drag & Drop ziehen
                        selectedItem = uiJunction;
                    }
                }
                else
                {
                    selectedPlug = null;
                    selectedItem = closestItem;
                }

                selectedUItemStroke = selectedItem.Stroke;
                selectedItem.Stroke = Brushes.Red;
            }

            if (Mouse.RightButton == MouseButtonState.Pressed)
            {
                var position = e.GetPosition(canvasMain);
                UIPart selectedItem = itc.Items
                   .OfType<UIPart>()
                   .OrderBy(part => (part.Part.Location - position).Length)
                   .FirstOrDefault();

                switch (selectedItem.Direction)
                {
                    case Direction.Unspec:
                        break;
                    case Direction.East:
                        selectedItem.Direction = Direction.South;
                        break;
                    case Direction.South:
                        selectedItem.Direction = Direction.West;
                        break;
                    case Direction.West:
                        selectedItem.Direction = Direction.North;
                        break;
                    case Direction.North:
                        selectedItem.Direction = Direction.East;
                        break;
                    default:
                        break;
                }
            }
        }

        public void CanvasMain_MouseMove_DragPart(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton != MouseButtonState.Pressed)
                return;

            var position = e.GetPosition(canvasMain);
            if (!mouseLastPosition.HasValue)
            {
                // beim ersten Ziehen nach Druck der Maus muss der Vektor auf 0 gesetzt werden,
                // sonst springt das Bauteil als wäre der normale Mausbetrieb Teil des Ziehvorgangs
                mouseLastPosition = position;
                return;
            }
            
            //Mausposition
            var dragVect = position - mouseLastPosition.Value;
            mouseLastPosition = position;

            //Part mit der kleinsten Entfernung zum Mauszeiger
            //var closest = itc.Items
            //    .OfType<UIPart>()
            //    .OrderBy(part => (part.Part.Location - position).Length)
            //    .FirstOrDefault();

            //null->keins gefunden
            //außerdem muss sich cursor innerhalb von 20px der Grafik aufhalten
            //if (closest != null && closest.Path.Data.FillContains(position, 20, ToleranceType.Absolute))
            //{
            //    closest.SetOffset(dragVect);
            //}

            selectedItem?.SetOffset(dragVect);
        }

        
        private void Save()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.Filter = "CSV-Datei | *.csv";
            if (sfd.ShowDialog() == true)
            {
                StreamWriter sw = new StreamWriter(sfd.FileName);

                foreach (UIPart item in PartCollection)
                {
                    //sw.WriteLine(item.Serialize());
                }
                sw.Close();
            }
        }

        private void Open()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "CSV-Datei | *.csv";
            if (ofd.ShowDialog() == true)
            {
                PartCollection.Clear();
                StreamReader sr = new StreamReader("C:/Users/Peter/Desktop/TestSave.csv");
                while (!sr.EndOfStream)
                {
                    string[] data = sr.ReadLine().Split(';');
                    Part part = Part.Parse(data);
                    UIPart uipart = new UIPart();

                    switch (data[0].ToString())
                    {
                        case "SchematiX.Model.Resistor":
                            PartCollection.Add(new UIResistor() { Part = part });
                            break;
                        case "SchematiX.Model.Capacitor":
                            PartCollection.Add(new UICapacitor() { Part = part });
                            break;
                        case "SchematiX.Model.Diode":
                            PartCollection.Add(new UIDiode() { Part = part });
                            break;
                        case "SchematiX.Model.Voltage":
                            PartCollection.Add(new UIVoltage() { Part = part });
                            break;
                        case "SchematiX.Model.Junction":
                            PartCollection.Add(new UIJunction() { Part = part });
                            break;
                        case "SchematiX.Model.Current":
                            PartCollection.Add(new UICurrent() { Part = part });
                            break;
                        case "SchematiX.Model.Elko":
                            PartCollection.Add(new UIElko() { Part = part });
                            break;
                        case "SchematiX.Model.FET":
                            PartCollection.Add(new UIFET() { Part = part });
                            break;
                        case "SchematiX.Model.NPN":
                            PartCollection.Add(new UINPN() { Part = part });
                            break;
                        case "SchematiX.Model.PNP":
                            PartCollection.Add(new UIPNP() { Part = part });
                            break;
                        //case "SchematiX.Model.TBipolar":
                        //    part = new UIResistor();
                        //    break;
                        //case "SchematiX.Model.TFieldEffect":
                        //    part = new ();
                        //    break;
                        default:
                            uipart = new UIPart();
                            break;
                    }
                }
                sr.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = PartCollection;
            itc.ItemsSource = PartCollection;
            //Save();
            Open();
        }

        private void cmbBox_newPart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = cmbBox_newPart.SelectedIndex;

            if (index != 0)
            {
                string[] tokens = cmbBox_newPart.Items[index].ToString().ToUpper().Split(' ');
                string part = tokens[1];

                switch (part)
                {
                    case "FET":
                        FET f = new FET()
                        {
                            Location = new Location(0, 0),
                            Direction = Direction.West
                        };

                        PartCollection.Add(new UIFET() { Part = f });
                        break;

                    case "NPN":
                        NPN npn = new NPN()
                        {
                            Direction = Direction.West,
                            Location = new Location(0, 0)
                        };

                        PartCollection.Add(new UINPN() { Part = npn });
                        break;

                    case "PNP":
                        PNP pnp = new PNP()
                        {
                            Direction = Direction.West,
                            Location = new Location(0, 0)
                        };

                        PartCollection.Add(new UIPNP() { Part = pnp });
                        break;

                    case "CAPACITOR":
                        Capacitor c = new Capacitor()
                        {
                            Location = new Location(20, 10)
                        };

                        PartCollection.Add(new UICapacitor() { Part = c });
                        break;

                    case "CURRENT":
                        Current crnt = new Current()
                        {
                            Location = new Location(40, 20),
                            Direction = Direction.South
                            //,
                            //Junctions = new List<Node> { n3, n4 }
                        };

                        PartCollection.Add(new UICurrent() { Part = crnt });
                        break;

                    case "DIODE":
                        Diode d = new Diode()
                        {
                            Location = new Location(0, 0)
                        };

                        PartCollection.Add(new UIDiode() { Part = d });
                        break;

                    case "ELKO":
                        Elko elko = new Elko()
                        {
                            Location = new Location(20, 10)
                        };

                        PartCollection.Add(new UIElko() { Part = elko });
                        break;

                    case "INDUCTOR":
                        Inductor i = new Inductor()
                        {
                            Location = new Location(10, 10)
                        };

                        PartCollection.Add(new UIInductor() { Part = i });
                        break;

                    case "RESISTOR":
                        Resistor r = new Resistor()
                        {
                            Location = new Location(0, 0),
                            Direction = Direction.West
                        };

                        PartCollection.Add(new UIResistor() { Part = r });
                        break;

                    case "VOLTAGE":
                        Voltage v = new Voltage()
                        {
                            Location = new Location(40, 40),
                            Direction=Direction.South
                            //,
                            //Junctions = new List<Node> { n8, n9 }
                        };

                        PartCollection.Add(new UIVoltage() { Part = v });
                        break;

                    default:
                        break;
                }
                cmbBox_newPart.SelectedIndex = 0;
            }
            else { /* nothing */ }
        }
    }
}
